#include "list.h"
#include "tree_.h"
#include "tree.h"
#include "hash_map.h"
#include "node.h"

using namespace data_structures;

int main() {
	/*
	Element el_1 = {nullptr, 9,  nullptr};
	Element el_2 = {nullptr, 16, nullptr};
	Element el_3 = {nullptr, 4,  nullptr};
	Element el_4 = {nullptr, 1,  nullptr};
	Element el_5 = {nullptr, 25, nullptr};
	//List list;
	//list.head = nullptr;
	List_with_sentinel list;
	list.insert(&el_4);
	list.insert(&el_3);
	list.insert(&el_2);
	list.insert(&el_1);
	cout << list << endl;
	list.insert(&el_5);
	cout << list << endl;
	list.remove(list.search(4));
	cout << list << endl;
	*/
	/*
	tree::Element el_0 = {nullptr, 0,  nullptr, nullptr};
	tree::Element el_11 = {nullptr, 11,  nullptr, nullptr};
	tree::Element el_12 = {nullptr, 12,  nullptr, nullptr};
	tree::Element el_31 = {nullptr, 31,  nullptr, nullptr};
	tree::Element el_32 = {nullptr, 32,  nullptr, nullptr};
	tree::Element el_34 = {nullptr, 34,  nullptr, nullptr};
	Binary_tree tree;
	tree.root = &el_0;
	
	tree.insert(tree.root, &el_11, false);
	tree.insert(tree.root, &el_12, true);
	
	tree.insert(&el_11, &el_31, false);
	tree.insert(&el_11, &el_32, true);
	tree.insert(&el_12, &el_34, true);
	
	cout << tree << endl;
	*/
	/*
	Element el_1 = {nullptr, 9,  nullptr};
	Element el_2 = {nullptr, 16, nullptr};
	Element el_3 = {nullptr, 4,  nullptr};
	Element el_4 = {nullptr, 1,  nullptr};
	Element el_5 = {nullptr, 25, nullptr};
	Chained_hash t;
	t.insert(&el_4);
	t.insert(&el_3);
	t.insert(&el_2);
	t.insert(&el_1);
	cout << t << endl;
	t.insert(&el_5);
	cout << t << endl;
	t.remove(t.search(4));
	cout << t << endl;
	return 0;
	*/
/*
   6
 4   7
2 5   8 
       10
      9  11
*/
	tree::Element two = {nullptr, 2,  nullptr, nullptr};
	tree::Element five = {nullptr, 5,  nullptr, nullptr};
	tree::Element eight = {nullptr, 8,  nullptr, nullptr};
	
	tree::Element four = {nullptr, 4, &five, &two};
	two.p = five.p = &four;
	tree::Element seven = {nullptr, 7, &eight, nullptr};
	eight.p = &seven;	
	
	tree::Element six = {nullptr, 6, &seven, &four};
	four.p = &six;
	seven.p = &six;
	
	Binary_search_tree::inorder_tree_walk(&six);
	std::cout << Binary_search_tree::iterative_tree_search(&six, 5) << ' ' << &five << std::endl;
	std::cout << Binary_search_tree::tree_minimum(&six)->k << std::endl;
	std::cout << Binary_search_tree::tree_maximum(&four)->k << std::endl;
	std::cout << Binary_search_tree::tree_successor(&four)->k << std::endl;
	std::cout << Binary_search_tree::tree_successor(&five)->k << std::endl;
	std::cout << Binary_search_tree::tree_predecessor(&six)->k << std::endl;
	std::cout << Binary_search_tree::tree_predecessor(&eight)->k << std::endl;

	tree::Element nine = {nullptr, 9, nullptr, nullptr};
	tree::Element ten = {nullptr, 10, nullptr, nullptr};
	Binary_search_tree::tree_insert(&six, &ten);
	Binary_search_tree::tree_insert(&six, &nine);
	std::cout << "insert\n";
	std::cout << eight.r->k << " " << ten.p->k << " " << ten.l->k << " " << nine.p->k << std::endl;

	// tree::Element one_h = {nullptr, 100, nullptr, nullptr};
	// Binary_search_tree::trancplant(&six, &eight, &one_h);
	// std::cout << std::endl;
	// Binary_search_tree::inorder_tree_walk(&six);
	std::cout << std::endl;
// 8
//	Binary_search_tree::tree_delete(&six, &eight);
//	std::cout << seven.r->k << " " << seven.r->l->k << std::endl;
// 10
//	Binary_search_tree::tree_delete(&six, &ten);
//	std::cout << eight.r->k << std::endl;
// 5
//	Binary_search_tree::tree_delete(&six, &five);
//	Binary_search_tree::inorder_tree_walk(&six);

	tree::Element eleven = {nullptr, 11, nullptr, nullptr};
	Binary_search_tree::tree_insert(&six, &eleven);
//	Binary_search_tree::left_rotate(&eight);
//	std::cout << seven.l << ' ' << seven.r->k << ' ' << seven.r->r->k << ' ' << seven.r->l->r->k << std::endl;

	tree::Element sto = {nullptr, 100, nullptr, nullptr};
	Binary_search_tree::tree_insert(&six, &sto);
	Binary_search_tree::print(&six);

	//data_structures::Node<uint32_t> node(5);
	//std::cout << node.key() << ' ' << bool(node) << std::endl;
	//data_structures::Node<uint32_t> node2(Node<uint32_t>::empty_node);
	data_structures::Tree<uint32_t> tree;
	/*tree.insert(10);
	tree.insert(15);
	tree.insert(5);
	tree.insert(20);
	tree.insert(30);*/
	
	tree.insert({ 5, 1, 6, 2 });
	std::cout << tree.height() << ' ' << tree.maximum() << ' ' << tree.search(2).key() << std::endl;
	tree.print();
}
