#include <algorithm>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <set>
#include <list>

using namespace std;

struct C;
ostream& operator<<(ostream& os, const C& c);

struct C {
	int x;
	int y;
	C(initializer_list<int> c) {
		auto it = c.begin();
		x = *it;
		++it;
		y = *it;
		cout << "C " << *this << endl;
	}
	~C() {
		cout << "D " << *this << endl;
	}
	C(const C& c) {
		x = c.x;
		y = c.y;
		cout << "CP " << *this << endl;
	}
	C(C&& c) {
		x = c.x;
		y = c.y;
		c.x = c.y = 0;
		cout << "CM " << *this << endl;
	}
	C& operator=(const C& c) {
		x = c.x;
		y = c.y;
		cout << "= " << *this << endl;
		return *this;
	}
	/*C& operator=(C&& c) {
		x = c.x;
		y = c.y;
		c.x = c.y = 0;
		cout << "=M " << *this << endl;
		return *this;
	}*/
};

ostream& operator<<(ostream& os, const C& c) {
	os << "(" << c.x << ", " << c.y << ")";
	return os;
}

struct Compare_C {
	bool operator()(const C& c_1, const C& c_2) {
		const int dist = 20;
		cout << "compare: " << c_1 << " and " << c_2 << endl;
		if (abs(c_1.x - c_2.x) < dist && abs(c_1.y - c_2.y) < dist)
			return true;
		else
			return false;
	}
};

/*
struct Hash {
	
};

int main() {
	//multiset<C, Compare_C> s;
	unordered_multiset<C, Compare_C
	s.insert({1, 3});
	s.insert({1, 2});
	s.insert({41, 1});
	s.insert({3, 1});
	copy(s.begin(), s.end(), ostream_iterator<C, char>(cout, ", "));
	cout << endl;
	auto range = s.equal_range({1, 3});
	for (auto el = range.first; el != range.second; ++el)
		cout << *el << endl;
}
*/

struct Compare_C_2 {
	bool operator()(const C& c_1, const C& c_2) {
		cout << "compare 2: " << c_1 << " and " << c_2 << endl;
		if (c_1.x >= c_2.x) // problem with >: !comp(a, b) && !comp(b, a)
			return true;
		else
			return false;
	}
};

void insert(list<set<C, Compare_C_2>>& s, C& c) {
	auto it = s.end();
	for (auto _s = s.begin(); _s != s.end(); ++_s) {
		if (any_of((*_s).begin(), (*_s).end(), [&c](const C& _c){ return Compare_C()(c, _c); })) {
			if (it == s.end()) {
				it = _s;
			} else {
				//move((*_s).begin() , (*_s).end(), inserter(*it, (*it).end()));
				for (set<C, Compare_C_2>::iterator /*auto*/ el = (*_s).begin(); el != (*_s).end(); ++el) {
					cout << typeid(el).name() << endl;
					C cc(move(*_s));
					//(*it).insert(move(*el));
					//(*it).insert((C&&)(*el));
				}
				auto __s = _s;
				--_s; // TODO: list ISO: Bidirectional iterators ???
				s.erase(__s);
			}
		}
	}
	if (it == s.end()) {
		s.emplace_back();
		s.back().insert(move(c));
	} else {
		(*it).insert(move(c));
	}
}

int main() {
	list<set<C, Compare_C_2>> s;
	{
	C c = {1, 3};
	
	insert(s, c);
	
	c = {1, 4};
	insert(s, c);
	
	c = {30, 4};
	insert(s, c);
	
	c = {60, 4};
	insert(s, c);
	
	c = {15, 4};
	insert(s, c);
	}
	for (auto& _s : s) {
		copy(_s.begin(), _s.end(), ostream_iterator<C, char>(cout, ", "));
		cout << endl;
	}
}
