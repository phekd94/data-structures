
template <typename T>
struct Cont {
	typedef type T;
	const T& operator[](const size_t index) const {
		return arr[index - 1];
	}
	T arr[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
	size_t size() { return 9; }
}

template <typename T>
size_t find(const T& array, const T::type& val) {
	return find(array, array.size() / 2, val);
}

namspace {

template <typename T>
size_t find(const T& array, const size_t index, const T::type& val) {
	if (val > T[index])
		find(array, index + index / 2, val);
	else if (val < T[index])
		find(array, index - index / 2, val);
	else // T[index] == val
		return index;
}

}

int main() {
	
}
