#pragma once

#include <iostream>
#include <vector>
#include <cmath>
#include <string>

#include <memory>
#include <type_traits>

using namespace std;

namespace data_structures {

namespace tree {

struct Element { // : public smart_pointers TODO shared due to p,r,l
	Element* p;
	int k;
	Element* r;
	Element* l;
	// operator > < TODO for k
	// static make_el() TODO
	// set_p,r,l + regular expression for change "->{p,r,l} = back_reference" to ".set_{p,r,l}(back_ref)"
	// print constr,destr
/*
juju a->p = x;
bobo z->r = z;
lala r->l = r;
*/
};

} // namespace tree

/*struct Binary_tree {
	tree::Element* root;
	void insert(tree::Element* x, tree::Element* y, bool r) {
		if (r)
			x->r = y;
		else
			x->l = y;
		y->p = x;
		y->r = y->l = nullptr;
	}
	friend void print(ostream& os, tree::Element* x) {
		if (x->l)
			print(os, x->l);
		if (x->r)
			print(os, x->r);
		os << x->k << endl;
	}
	friend ostream& operator<<(ostream& os, const Binary_tree& bt) {
		if (bt.root)
			print(os, bt.root);
		os << endl;
		
		int level = 1;
		vector<tree::Element*> row;
		if (bt.root)
			row.push_back(bt.root);
		while(row.size()) {
			vector<tree::Element*> _row;
			os << endl << "===" << level++ << "===" << endl;
			for (auto el : row) {
				os << el->k << ' ';
				if (el->l)
					_row.push_back(el->l);
				if (el->r)
					_row.push_back(el->r);
			}
			row = _row;
		}
		return os;
	}
};*/

namespace Binary_search_tree {
	void inorder_tree_walk(tree::Element* x) {
		if (x) {
			inorder_tree_walk(x->l);
			std::cout << x->k << std::endl;
			inorder_tree_walk(x->r);
		}
	}
	tree::Element* iterative_tree_search(tree::Element* x, int k) {
		while (x && x->k != k)
			if (x->k < k)
				x = x->r;
			else
				x = x->l;
		return x;
	}
	tree::Element* tree_minimum(tree::Element* x) {
		while (x->l)
			x = x->l;
		return x;
	}
	tree::Element* tree_maximum(tree::Element* x) {
		while (x->r)
			x = x->r;
		return x;
	}
	tree::Element* tree_successor(tree::Element* x) {
		if (x->r)
			return tree_minimum(x->r);
		while (x->p && x == x->p->r)
			x = x->p;
		return x->p;
	}
	tree::Element* tree_predecessor(tree::Element* x) {
		if (x->l)
			return tree_maximum(x->l);
		while (x->p && x == x->p->l)
			x = x->p;
		return x->p;
	}
	void tree_insert(tree::Element* x, tree::Element* z) {
		tree::Element* y = nullptr;
		tree::Element* root = x;
		while (x) {
			y = x;
			x = (z->k > x->k ? x->r : x->l);
		}
		z->p=y;
		if (!root)
			; // empty tree
		else if (z->k > y->k)
			y->r = z;
		else
			y->l = z;
	}
	void trancplant(tree::Element* x, tree::Element* u, tree::Element* v) {
		if (!u->p)
			x = v;
		else if (u == u->p->l)
			u->p->l = v;
		else // u == u->p->r
			u->p->r = v;
		if (v)
			v->p = u->p;
	}
	void tree_delete(tree::Element* x, tree::Element* z) {
		if (!z->l)
			trancplant(x, z, z->r);
		else if (!z->r)
			trancplant(x, z, z->l);
		else {
			auto y = tree_minimum(z->r);
			if (y->p != z) {
				trancplant(x, y, y->r);
				y->r = z->r;
				y->r->p = y;
			}
			trancplant(x, z, y);
			y->l = z->l;
			y->l->p = y;
		}
	}
	void left_rotate(tree::Element* x) {
		auto y = x->r;
		
		// betta
		x->r = y->l;
		if (x->r)
			x->r->p = x;
		
		// parent
		y->p = x->p;
		if (!x->p)
			; // T.root = y;
		else if (x->p->r == x)
			x->p->r = y;
		else // x->p->l == x
			x->p->l = y;

		// x, y
		x->p = y;
		y->l = x;
	}
	void right_rotate(tree::Element* y) { // didn't test
		auto x = y->l;

		// betta
		y->l = x->r;
		if (y->l)
			y->l->p = y;

		// parent
		x->p = y->p;
		if (!y->p)
			; // T.root = x;
		else if (y->p->r == y)
			y->p->r = x;
		else // y->p->l == y
			y->p->l = x;

		// x, y
		y->p = x;
		x->r = y;
	}
	uint32_t get_height(tree::Element* x) {
		if (!x)
			return 0;
		auto l_h = get_height(x->l);
		auto r_h = get_height(x->r);
		return std::max(l_h, r_h) + 1;
	}
	void print(int32_t l, const std::vector<tree::Element*>& xs) {
		if (l < 0)
			return;
		std::vector<tree::Element*> es;
		const int width = 2;
		std::cout << std::string(width * (std::pow(2, l) - 1), ' ');
		for (auto x : xs) {
			std::cout.width(width);
			std::cout << (x ? std::to_string(x->k) : std::string("*"))
			          << std::string(width * (std::pow(2, l + 1) - 1), ' ');
			es.push_back(x ? x->l : nullptr);
			es.push_back(x ? x->r : nullptr);
		}
		std::cout << std::endl;
		print(l - 1, es);
	}
	void print(tree::Element* x) {
		print(get_height(x) - 1, {x});
	}
};

} // namespace data_structures
