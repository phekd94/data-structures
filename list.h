#pragma once

#include <iostream>

using namespace std;

namespace data_structures {

struct Element {
	Element* prev;
	int key;
	Element* next;
};

struct List {
	Element* head;
	Element* search(const int k) {
		auto x = head;
		while (x && x->key != k)
			x = x->next;
		return x;
	}
	void insert(Element* x) {
		x->next = head;
		if (head)
			head->prev = x;
		head = x;
		x->prev = nullptr;
	}
	void remove(Element* x) {
		if (x->prev)
			x->prev->next = x->next;
		else
			head = x->next;
		if (x->next)
			x->next->prev = x->prev;
	}
	friend ostream& operator<<(ostream& os, const List& l) {
		os << "head -> ";
		for (auto x = l.head; x; x = x->next)
			os << x->key << " -> ";
		os << "end of list";
		return os;
	}
};

struct List_with_sentinel {
	Element _nil;
	Element* nil = &_nil;
	List_with_sentinel() { nil->next = nil; nil->prev = nil; }
	Element* search(const int k) {
		auto x = nil->next;
		while (x != nil && x->key != k)
			x = x->next;
		return x;
	}
	void insert(Element* x) {
		x->next = nil->next;
		x->prev = nil;
		nil->next->prev = x;
		nil->next = x;
	}
	void remove(Element* x) {
		x->next->prev = x->prev;
		x->prev->next = x->next;
	}
	friend ostream& operator<<(ostream& os, const List_with_sentinel& l) {
		os << "head -> ";
		for (auto x = l.nil->next; x != l.nil; x = x->next)
			os << x->key << " -> ";
		os << "end of list";
		return os;
	}
};

} // namespace data_structures
