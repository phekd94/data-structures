#pragma once

#include <cmath>     // floor()
#include <memory>    // unique_ptr
#include <algorithm> // copy_n()
#include <utility>   // swap()

namespace AlgorithmsBook
{
	// Insertrion sort
	template <typename Arr_t, typename Size_t>
	void insertion_sort(Arr_t * const arr, const Size_t size)
	{
		// Check the incoming parameters
		if (nullptr == arr || size < 2)
			return;
		
		// Sort
		for (Size_t j {1}; j < size; ++j)
		{
			Arr_t key {arr[j]};
			
			for (Size_t i {j - 1}; ; --i)
			{
				if (arr[i] > key)
				{
					arr[i + 1] = arr[i];
				}
				else
				{
					arr[i + 1] = key;
					break;
				}

				if (0 == i)
				{
					arr[0] = key;
                    break;
				}
			}
		}
	}

	// Insertion sort for list
	
	// Merge sort
	template <typename Arr_t, typename Index_t>
	void merge_sort(Arr_t * const arr, const Index_t begin, const Index_t end)
	{
		// Merge function
		auto merge = [arr](const Index_t begin_1, const Index_t end_1, const Index_t end_2)
		{
			// Function for allocate memory for temporary array part and copy data
			auto getTmpArr = [arr](
				const Index_t index, const Index_t size, std::unique_ptr<Arr_t> & tmpArr)
			{
				tmpArr = std::unique_ptr<Arr_t>(new Arr_t[size]);
				std::copy_n(arr + index, size, tmpArr.get());
			};
			
			// Calculate sizes of array parts and get temporary arrays
			const Index_t n_1 {end_1 - begin_1 + 1};
			const Index_t n_2 {end_2 - end_1};
			std::unique_ptr<Arr_t> arr_1, arr_2;
			getTmpArr(begin_1, n_1, arr_1);
			getTmpArr(end_1 + 1, n_2, arr_2);
			
			// Sort
			Arr_t * const arr_1_p {arr_1.get()}, * const arr_2_p {arr_2.get()};
			Index_t i {0}, j {0};
			while (i < n_1 && j < n_2)
				if (arr_1_p[i] < arr_2_p[j])
				{
					arr[begin_1 + i + j] = arr_1_p[i];
					++i;
				}
				else
				{
					arr[begin_1 + i + j] = arr_2_p[j];
					++j;
				}
			if (n_1 == i)
				std::copy_n(arr_2_p + j, n_2 - j, arr + begin_1 + i + j);
			else
				std::copy_n(arr_1_p + i, n_1 - i, arr + begin_1 + i + j);
		};
		
		// Check the incoming parameters and sort
		if (arr != nullptr && begin < end)
		{
			Index_t q {static_cast<Index_t>(std::floor((begin + end) / 2.0))};
			merge_sort(arr, begin, q);
			merge_sort(arr, q + 1, end);
			merge(begin, q, end);
		}
	}
	
	// Bubble sort
	template <typename Arr_t, typename Size_t>
	void bubble_sort(Arr_t * const arr, const Size_t size)
	{
		// Check the incoming parameters
		if (nullptr == arr || size < 2)
			return;
		
		// Sort
		for (Size_t i {0}; i < size - 1; ++i)
			for (Size_t j {size - 1}; j > i; --j)
				if (arr[j] < arr[j - 1])
					std::swap(arr[j], arr[j - 1]);
	}
	
} // namespace AlgorithmsBook
