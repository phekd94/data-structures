#pragma once

// #include "debug.h"

#include <cassert>
#include <functional>
#include <iostream>
#include <memory>
#include <type_traits>

namespace data_structures {

template <typename T>
class Node;
template <typename T>
std::ostream& operator<<(std::ostream& os, const Node<T>& node);

template <typename T>
class Node {
 static_assert(std::is_copy_constructible_v<T> && std::is_nothrow_copy_constructible_v<T>,
               "Node requires nothrow copy contructor");
 static_assert(std::is_copy_assignable_v<T> && std::is_nothrow_copy_assignable_v<T>,
               "Node requires nothrow copy assign");
 // TODO static_assert(Debug.get() or init());
 public:
  explicit Node(const T& key);
  Node(const Node<T>& node);
  ~Node();

  const T& key() const { return m_key; }
  void set_key(const T& key);

  Node<T>& l() { return const_cast<Node<T>&>(const_cast<const Node<T>*>(this)->l()); }
  const Node<T>& l() const;
  void set_l(const T& key) { set_(m_left, key); }

  Node<T>& r() { return const_cast<Node<T>&>(const_cast<const Node<T>*>(this)->r()); }
  const Node<T>& r() const;
  void set_r(const T& key) { set_(m_right, key); }

  void set_p(const Node<T>& node);

  operator bool() const { return m_is_not_empty; }
  friend std::ostream& operator<<<>(std::ostream& os, const Node<T>& node);

  inline static Node<T> empty_node;
 private:
  Node() : m_key(0), m_is_not_empty(false) { }

  void set_(std::unique_ptr<Node<T>>& node, const T& key);

  T m_key;
  std::unique_ptr<Node<T>> m_left;
  std::unique_ptr<Node<T>> m_right;
  //std::unique_ptr<Node<T>> m_parent { nullptr, [](auto p) { std::cout << p << std::endl; } }; TODO
  // std::reference_wrapper<Node<T>> m_parent { empty_node };
  Node<T>* m_parent { nullptr };
  bool m_is_not_empty { true };
};

//-------------------------------------------------------------------------------------------------
template <typename T>
Node<T>::Node(const T& key) : m_key(key) {
	std::cout << "Node: " << *this << " is created" << std::endl;
}

//-------------------------------------------------------------------------------------------------
template <typename T>
Node<T>::Node(const Node<T>& node) : m_key(0), m_is_not_empty(false) {
	assert(&node == &Node<T>::empty_node);
	std::cout << "Empty node: " << *this << " is created" << std::endl;
}

//-------------------------------------------------------------------------------------------------
template <typename T>
Node<T>::~Node() {
	std::cout << (m_is_not_empty ? "N" : "Empty n") << "ode: " << *this << " is deleted" << std::endl;
}

//-------------------------------------------------------------------------------------------------
template <typename T>
std::ostream& operator<<(std::ostream& os, const Node<T>& node) {
	return os << "address = " << &node << ", key = " << node.m_key;
}

//-------------------------------------------------------------------------------------------------
template <typename T>
void Node<T>::set_key(const T& key) {
	assert(this != &Node<T>::empty_node);
	assert(!m_is_not_empty);
	m_key = key;
	m_is_not_empty = true;
}

//-------------------------------------------------------------------------------------------------
template <typename T>
const Node<T>& Node<T>::l() const {
	return m_left ? *m_left : empty_node;
}

//-------------------------------------------------------------------------------------------------
template <typename T>
const Node<T>& Node<T>::r() const {
	return m_right ? *m_right : empty_node;
}

//-------------------------------------------------------------------------------------------------
template <typename T>
void Node<T>::set_p(const Node<T>& node) {
	assert(node);
	std::cout << "Set parent: " << node << " for node: " << *this << std::endl;
	m_parent = &const_cast<Node<T>&>(node);
}

//-------------------------------------------------------------------------------------------------
template <typename T>
void Node<T>::set_(std::unique_ptr<Node<T>>& node, const T& key) {
	assert(!node);
	node = std::make_unique<Node<T>>(key);
	std::cout << "Set " << (&node == &m_left ? "left: " : "right: ") << *node << " for node: " << *this << std::endl;
}

} // namespace data_structures
