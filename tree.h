#pragma once

#include "node.h"

#include <climits>
#include <cmath>
#include <initializer_list>
#include <iostream>
#include <functional>
#include <list>
#include <utility>

namespace data_structures {

//-------------------------------------------------------------------------------------------------
template <typename T>
class Tree {
 public:
  void print() const { print(height(), { std::cref(m_root) }); }
  unsigned int height() const { return height(m_root); }
  void insert(const T& key);
  void insert(std::initializer_list<std::reference_wrapper<const T>> keys);
  void insert(std::initializer_list<T> keys);
  const T& maximum() const { return min_max(false); }
  const T& minimum() const { return min_max(true); }
  Node<T>& search(const T& key);
  //void del(Node<T>& z);
 private:
  void print(const decltype(std::declval<Tree>().height()) l, // TODO https://en.cppreference.com/w/cpp/utility/declval  std::add_rvalue_reference ???
             const std::list<std::reference_wrapper<const Node<T>>>& row) const; // TODO https://stackoverflow.com/questions/922360/why-cant-i-make-a-vector-of-references
  unsigned int height(const Node<T>& x) const;
  const T& min_max(bool minimum) const;
  void transplant(const Node<T>& u, const Node<T>& v);
  Node<T> m_root { Node<T>::empty_node };
};

//-------------------------------------------------------------------------------------------------
template <typename T>
void Tree<T>::print(const decltype(std::declval<Tree>().height()) l,
                    const std::list<std::reference_wrapper<const Node<T>>>& row) const {
	std::list<std::reference_wrapper<const Node<T>>> new_row;
	const int width = std::numeric_limits<T>::is_integer ? 
	                    [this]() { 
	                      unsigned int number_of_digits = 0;
	                      auto n = maximum();
	                      do {
	                        ++number_of_digits;
	                        n /= 10;
	                      } while (n);
	                      return number_of_digits;
	                    }() : 2;
	std::cout << std::string(width * (std::pow(2, l - 1) - 1), ' ');
	for (auto node : row) {
		std::cout.width(width);
		std::cout << (node.get() ? std::to_string(node.get().key()) : std::string("*"))
		          << std::string(width * (std::pow(2, l) - 1), ' ');
		new_row.emplace_back(std::cref( node.get() ? node.get().l() : Node<T>::empty_node ));
		new_row.emplace_back(std::cref( node.get() ? node.get().r() : Node<T>::empty_node ));
	}
	std::cout << std::endl;
	if (l - 1 != 0)
		print(l - 1, new_row);
}

//-------------------------------------------------------------------------------------------------
template <typename T>
unsigned int Tree<T>::height(const Node<T>& node) const {
	if (!node)
		return 0;
	const auto l_h = height(node.l());
	const auto r_h = height(node.r());
	return std::max(l_h, r_h) + 1;
}

//-------------------------------------------------------------------------------------------------
template <typename T>
void Tree<T>::insert(const T& key) {
	// First way
	auto y = std::ref(Node<T>::empty_node);
	auto x = std::ref(m_root);
	while (x.get()) {
		y = x;
		x = (key > x.get().key() ? std::ref(x.get().r()) : std::ref(x.get().l()));
	}
	
	// Second way
	/*
	std::function< std::pair<reference_wrapper<Node<T>>, reference_wrapper<Node<T>>>(const Node<T>& x, const Node<T>& y) > find = 
	[&find, &key](const Node<T>& x, const Node<T>& y) -> std::pair<reference_wrapper<Node<T>>, reference_wrapper<Node<T>>> {
		if (!x)
			return std::make_pair(std::ref(const_cast<Node<T>&>(x)), std::ref(const_cast<Node<T>&>(y)));
		else
			return find(key > x.key() ? x.r() : x.l(), x);
	};
	auto [x, y] = find(m_root, Node<T>::empty_node);
	*/
	
	// Third way
	// http://pedromelendez.com/blog/2015/07/16/recursive-lambdas-in-c14/

	if (!y.get()) {
		m_root.set_key(key);
	} else {
		if (key > y.get().key()) {
			y.get().set_r(key);
			y.get().r().set_p(y);
		} else {
			y.get().set_l(key);
			y.get().l().set_p(y);
		}
	}
}

//-------------------------------------------------------------------------------------------------
template <typename T>
void Tree<T>::insert(std::initializer_list<std::reference_wrapper<const T>> keys) {
	for (const auto& key : keys)
		insert(key.get());
}

//-------------------------------------------------------------------------------------------------
template <typename T>
void Tree<T>::insert(std::initializer_list<T> keys) {
	for (const auto& key : keys)
		insert(key);
}

//-------------------------------------------------------------------------------------------------
template <typename T>
const T& Tree<T>::min_max(bool minimum) const {
	if (!m_root)
		throw std::runtime_error("Tree is empty");
	auto x = std::cref(m_root);
	if (minimum)
		while (x.get().l())
			x = std::cref(x.get().l());
	else
		while (x.get().r())
			x = std::cref(x.get().r());
	return x.get().key();
}

//-------------------------------------------------------------------------------------------------
template <typename T>
Node<T>& Tree<T>::search(const T& key) {
	auto x = std::ref(m_root);
	while (x.get() && x.get().key() != key)
		if (x.get().key() > key)
			x = std::ref(x.get().l());
		else
			x = std::ref(x.get().r());
	if (!x.get())
		throw std::runtime_error("Key not found");
	return x.get();
}

//-------------------------------------------------------------------------------------------------
/*template <typename T>
void Tree<T>::del(Node<T>& z) {
	auto transplant = [&](... u, ... v) {
		u.p().
	};
	if (!z.l()) {
		
	}
}*/

} // namespace data_structures
