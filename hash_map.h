#pragma once

#include <iostream>

#include "list.h"

using namespace std;

namespace data_structures {

struct Chained_hash {
	constexpr static const int mod = 3;
	List_with_sentinel table[mod];
	Element* search(const int k) {
		return table[k % mod].search(k);
	}
	void insert(Element* x) {
		table[x->key % mod].insert(x);
	}
	void remove(Element* x) {
		table[x->key % mod].remove(x);
	}
	friend ostream& operator<<(ostream& os, const Chained_hash& h) {
		for (int i = 0; i < mod; ++i)
			cout << i << ": " << h.table[i] << endl;
		return os;
	}
};

} // namespace data_structures
